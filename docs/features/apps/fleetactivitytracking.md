# Fleet Activity Tracking

The Fleet Activity Tracking (FAT) app allows you to track fleet participation.

![fat](/_static/images/features/apps/fat.png)

## Installation

Fleet Activity Tracking requires access to the `esi-location.read_location.v1`, `esi-location.read_ship_type.v1`, and `esi-universe.read_structures.v1` SSO scopes. Update your application on the [EVE Developers site](https://developers.eveonline.com) to ensure these are available.

Add `'allianceauth.fleetactivitytracking',` to `INSTALLED_APPS` in your `local.py`

Perform Django Maintenance and restart our Web Service and Workers.

::::{tabs}
:::{group-tab} Bare Metal

```shell
python manage.py migrate
python manage.py collectstatic --noinput
supervisorctl restart myauth:
```

:::
:::{group-tab} Containerized

```shell
docker compose --env-file=.env up -d
docker compose exec allianceauth_gunicorn bash
auth migrate
auth collectstatic
```

:::
::::

## Permissions

To administer this feature, users will require some of the following.

Users do not require any permissions to interact with FAT Links created.

```{eval-rst}
+---------------------------------------+------------------+--------------------------------------------------------------------------+
| Permission                            | Admin Site       | Auth Site                                                                |
+=======================================+==================+==========================================================================+
| auth.fleetactivitytracking            | None             | Create and Modify FATLinks                                               |
+---------------------------------------+------------------+--------------------------------------------------------------------------+
| auth.fleetactivitytracking_statistics | None             | Can view detailed statistics for corp models and other characters.       |
+---------------------------------------+------------------+--------------------------------------------------------------------------+
```
